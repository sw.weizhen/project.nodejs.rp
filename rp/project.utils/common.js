const crypto = require('crypto');

function respObj(code, msg, data) {
    let obj = new Object();

    obj.code = code;
    obj.msg = msg;
    obj.data = data;

    return obj;
}

function cryptoPwd(pwd) {
    return crypto.createHash('sha256').update(pwd).digest('hex');
}

function getRandomnum (n) {
	var chars = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' ];
	var res = '';
	for ( var i = 0; i < n; i++ ) {
		var id = Math.ceil( Math.random() * 35 );
		res += chars[ id ];
	}
	return res;
};

function convertTimestamp(timestamp) {

    let ts = timestamp * 1000;
    
    let date = new Date(ts);
    return  date.getUTCFullYear() + "-" +
            (date.getUTCMonth() + 1) + "-" + 
            date.getUTCDate() + " " +
            date.getUTCHours() + ":" + 
            date.getUTCMinutes() + ":" + 
            date.getUTCSeconds();
}

function checkSpecialCharacters(inputStr) {
    let format = /[`!@#$%^&*()+=\[\]{};':"\\|,.<>\/?~]/;

    return format.test(inputStr);
}


function checkIfEmpty(inputStr) {
    if((typeof inputStr) === 'undefined' || inputStr === null) {
        return true;
    }
    
    return false;
}

function checkIfAllNumbers(inputStr) {

    let format = /^(\-|\+)?\d+(\.\d+)?$/;

    if((typeof inputStr) === 'undefined' || inputStr === null) {
        return false;
    }

    try {
    
        let strVal = inputStr.toString();
        if(strVal.length == 0) {
            return false;
        }

        return format.test(inputStr);

    } catch(err) {
        console.log(err);

        return false;
    }
}

function checkStrLen(inputVal, minLen, maxLen) {
    try {
        if(minLen > maxLen) {
            throw '我她媽';
        }

        if((typeof inputVal) === 'undefined' || inputVal === null) {
            return false;
        }

        let strVal = inputVal.toString();
        let strValLen = strVal.length;

        if((strValLen < minLen) || (strValLen > maxLen)) {
            return false;
        }

        return true;

    } catch(err) {
        console.log('靠ㄅ' + err);

        return false;
    }
}

const isNumber = n => (typeof(n) === 'number' || n instanceof Number);

module.exports = {
    respObj: respObj,
    cryptoPwd: cryptoPwd,
    isNumber: isNumber,
    getRandomnum: getRandomnum,
    convertTimestamp: convertTimestamp,
    checkSpecialCharacters: checkSpecialCharacters,
    checkIfAllNumbers: checkIfAllNumbers,
    checkIfEmpty: checkIfEmpty,
    checkStrLen: checkStrLen
};