const cfg = {

    server_port: 3001,
    server_key: 'a!!OpQM}#(tssw)&e',
    token_expire: 3600*24*3,

    cors_ip: ['http://192.168.33.84:81', 'http://192.168.33.83:81', 'http://localhost:3000', 'http://192.168.35.14:3000', 'http://192.168.35.14:2023', 'http://localhost:2023'],

    mysql: {
        host: '192.168.33.84',
        port: 3306,
        user: 'root',
        password: '123456',
        database: 'rp',
        multipleStatements: true,
        waitForConnections: true,
        connectionLimit: 10,
        queueLimit: 0
    },

    redis: {
        host: '192.168.33.84',
        port: 6050
        // host: '192.168.35.18',
        // port: 6379
    },

}

module.exports = {
    cfg
};