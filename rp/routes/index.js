var express = require('express');
var router = express.Router();

var mdwAuth = require('../project.middleware/auth');
var mdwPermission = require('../project.middleware/permission');

var apiLogin = require('../project.api/login');
var apiMenu = require('../project.api/menu');
var apiPower = require('../project.api/power');
var apiRole = require('../project.api/role');
var apiUser = require('../project.api/user');
var apiCompany = require('../project.api/company');
var apiProvider = require('../project.api/provider');
var apiGame = require('../project.api/game');
var apiSysLog = require('../project.api/syslog');

router.post('/test', apiLogin.test);
router.post('/platform/auth',  apiLogin.login);
router.post('/platform/auth/totp',  apiLogin.verifyTOTP);

router.get('/platform/self',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiMenu.selfMenu);

router.get('/platform/menu',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiPower.getPower);
router.post('/platform/menu',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiPower.addPower);
router.put('/platform/menu',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiPower.modifyPower);
router.delete('/platform/menu/:id',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiPower.delPower);

router.get('/platform/role',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiRole.getRole);
router.post('/platform/role',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiRole.addRole);
router.put('/platform/role',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiRole.modifyRole);
router.delete('/platform/role/:id',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiRole.delRole);

router.get('/platform/user',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiUser.getUser);
router.post('/platform/user',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiUser.addUser);
router.put('/platform/user',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiUser.modifyUserProfile);
router.put('/platform/user/password',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiUser.modifyUserPwd);
router.put('/platform/user/totp/:id',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiUser.modifyUserTOTP);
router.delete('/platform/user/:id',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiUser.delUser);

router.get('/platform/company',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiCompany.getCompany);
router.post('/platform/company',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiCompany.addCompany);
router.put('/platform/company',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiCompany.modifyCompany);
router.delete('/platform/company/:companyId',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiCompany.delCompany);

router.get('/platform/provider',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiProvider.getProvider);
router.post('/platform/provider',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiProvider.addProvider);
router.put('/platform/provider',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiProvider.modifyProvider);
router.delete('/platform/provider/:providerId',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiProvider.delProvider);

router.get('/platform/game',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiGame.getGame);
router.post('/platform/game',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiGame.addGame);
router.put('/platform/game',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiGame.modifyGame);
router.delete('/platform/game/:gameId',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiGame.delGame);

router.get('/platform/sysLogs',  mdwAuth.authenticate, mdwPermission.permissionCtrl, apiSysLog.getSyslog);


module.exports = router;