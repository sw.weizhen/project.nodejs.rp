
const db = require('../project.modules/mysql_instance');
const cache = require('../project.modules/redis_instance');
const utils = require('../project.utils/common');

//  platform/self
async function selfMenu(req, res) {

    let conn = await db.getConnection();
    try {
        let name = req.userObj.name;
        if (!name) {
            throw 'username undefined';
        }
        
        let rows = await conn.execute("SELECT roleId FROM RP_User WHERE name = ? AND isDelete = 0;", [name]);
        if (rows[0].length <= 0) {
            return res.status(200).json(
                utils.respObj(5900, "USER_NOT_EXIST", null)
            );    
        }

        let roleId = rows[0][0].roleId;

        const sqlSyntax = 'SELECT rp.id, rp.pid, rp.`action` , rp.name, rp.mark, rp.sort, rp.status ' + 
                          'FROM RP_Power_Role rpr ' + 
                          'INNER JOIN RP_Power rp ' +
                          'ON rpr.rpPowerId = rp.id ' + 
                          'AND rp.isDelete = 0 ' + 
                          'WHERE rpr.rpRoleId = ?;';

        rows = await conn.execute(sqlSyntax, [roleId]);
        
        return res.status(200).json(
            utils.respObj(0, null, rows[0])
        );

    } catch(err) {
        console.log(err);

        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );
    
    } finally {
        console.log('menu release');

        conn.release();
    }
    
}

module.exports = {
    selfMenu
}