const db = require('../project.modules/mysql_instance');
const sl = require('../project.api/syslog');
const utils = require('../project.utils/common');


//  platform/company
async function getCompany(req, res) {

    let page = req.query.page;
    let pageSize = req.query.pageSize;
    if(!page || !pageSize) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", null)
        );
    }

    let nPage = parseInt(page);
    let nPageSize = parseInt(pageSize);
    let start = nPage * nPageSize;

    if (isNaN(start)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", null)
        );
    }

    let companyId = req.query.companyId;
    let serverId = req.query.serverId;
    let status = req.query.status;
    
    let conn = await db.getConnection();
    try {
        let condition = '';
        let conditionVal = [];
        let obj = new Object();
        obj.count = 0;
        obj.rows = [];

        if(!utils.checkIfEmpty(companyId)) {
            if(!utils.checkIfAllNumbers(companyId)) {
                obj.illegalParam = "companyId";
                return res.status(200).json(
                    utils.respObj(1051, "PARAM_TYPE_ERROR", obj)
                );
            }

            if(!utils.checkStrLen(companyId, 1, 15)) {
                obj.lengthLimitParam = "companyId";
                return res.status(200).json(
                    utils.respObj(1054, "PARAM_LEN_LIMIT", obj)
                );
            }

            condition += 'AND cp.companyId = ? ';
            conditionVal.push(companyId);
        }

        if(!utils.checkIfEmpty(serverId)) {
            if(!utils.checkIfAllNumbers(serverId)) {
                obj.illegalParam = "serverId";
                return res.status(200).json(
                    utils.respObj(1051, "PARAM_TYPE_ERROR", obj)
                );
            }

            if(!utils.checkStrLen(serverId, 1, 15)) {
                obj.lengthLimitParam = "serverId";
                return res.status(200).json(
                    utils.respObj(1054, "PARAM_LEN_LIMIT", obj)
                );
            }

            condition += 'AND cp.serverId = ? ';
            conditionVal.push(serverId);
        }

        if(!utils.checkIfEmpty(status)) {
            if(!utils.checkIfAllNumbers(status)) {
                obj.illegalParam = "status";
                return res.status(200).json(
                    utils.respObj(1051, "PARAM_TYPE_ERROR", obj)
                );
            }

            if(!utils.checkStrLen(status, 1, 5)) {
                obj.lengthLimitParam = "status";
                return res.status(200).json(
                    utils.respObj(1054, "PARAM_LEN_LIMIT", obj)
                );
            }

            condition += 'AND cp.status = ? ';
            conditionVal.push(status);
        }

        let cntSql = 'SELECT count(*) AS cnt FROM Company cp WHERE cp.isDelete = 0 ' + condition + ';';

        let rowCnt = await conn.execute(cntSql, conditionVal);

        let sqlSel = "SELECT cp.id, cp.companyId, cp.serverId, cp.companyNickname, cp.providerAgentIds, cp.desKey, cp.md5Key, cp.whiteip, cp.status, cp.createUser, DATE_FORMAT(cp.createTime, '%Y-%m-%d %T') AS createTime, cp.lastEditUser, DATE_FORMAT(cp.lastEditTime, '%Y-%m-%d %T') AS lastEditTime " +
                     'FROM Company cp WHERE cp.isDelete = 0 ' + condition + ' LIMIT ?, ?;';
        
        conditionVal.push(start.toString());
        conditionVal.push(pageSize.toString());

        let rows = await conn.execute(sqlSel, conditionVal);
        
        
        obj.count = rowCnt[0][0].cnt;
        obj.rows = rows[0];

        return res.status(200).json(
            utils.respObj(0, null, obj)
        );

    } catch(err) {
        
        console.log(err);

        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );

    } finally {
        console.log('getCompany release');

        conn.release();
    }
}


//  platform/company
async function addCompany(req, res) {

    let companyId = req.body.companyId;
    let companyNickname = req.body.companyNickname;

    if(utils.checkIfEmpty(companyId)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "companyId")
        );
    }

    if(!utils.checkIfAllNumbers(companyId)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "companyId")
        );
    }

    if(!utils.checkStrLen(companyId, 1, 15)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "companyId")
        );
    }

    if(utils.checkIfEmpty(companyNickname)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "companyNickname")
        );
    }
    
    if (utils.checkSpecialCharacters(companyNickname)) {
        return res.status(200).json(
            utils.respObj(1053, "ILLEGAL_CHARACTER", "companyNickname")
        );
    }

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {

        let sqlCnt = 'SELECT count(*) AS cnt FROM Company WHERE companyId = ?;';
        let rowCnt = await conn.execute(sqlCnt, [companyId]);
        if (rowCnt[0][0].cnt !== 0) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(1105, 'COMPANY_EXIST', companyId)
            );  
        } 

        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }

        let desKey = utils.getRandomnum(16);
        let md5Key = utils.getRandomnum(16);

        let sqlInsert = 'INSERT INTO Company(companyId, companyNickname, desKey, md5Key, createUser, createTime, lastEditUser, lastEditTime, isDelete)' + 
                        'VALUES(?, ?, ?, ?, ? , utc_timestamp(), ?, utc_timestamp(), 0);';
        
        let sqlVal = [companyId, companyNickname, desKey, md5Key, username, username];

        let resp = await conn.execute(sqlInsert, sqlVal);
        let id = resp[0].insertId;

        let sqlSel = "SELECT cp.id, cp.companyId, cp.serverId, cp.companyNickname, cp.providerAgentIds, cp.desKey, cp.md5Key, cp.whiteip, cp.status, cp.createUser, DATE_FORMAT(cp.createTime, '%Y-%m-%d %T') AS createTime, cp.lastEditUser, DATE_FORMAT(cp.lastEditTime, '%Y-%m-%d %T') AS lastEditTime " +
                     'FROM Company cp WHERE cp.id = ?;';
        
        let sqlSelVal = [id];

        let row = await conn.execute(sqlSel, sqlSelVal);
        
        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
        await conn.execute(sl.sqlSyslog, sqlLogVal);

        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, row[0][0])
        );   

    } catch(err) {
        
        console.log(err);

        await conn.rollback();
        
        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );

    } finally {
        console.log('addCompany release');

        conn.release();
    }

}


//  platform/company
async function modifyCompany(req, res) {

    let companyId = req.body.companyId;
    let companyNickname = req.body.companyNickname;
    let whiteip = req.body.whiteip;

    if(utils.checkIfEmpty(companyId)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "companyId")
        );
    }

    if(!utils.checkIfAllNumbers(companyId)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "companyId")
        );
    }

    if(!utils.checkStrLen(companyId, 1, 15)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "companyId")
        );
    }

    if(utils.checkIfEmpty(companyNickname)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "companyNickname")
        );
    }

    if (utils.checkSpecialCharacters(companyNickname)) {
        return res.status(200).json(
            utils.respObj(1053, "ILLEGAL_CHARACTER", "companyNickname")
        );
    }
    
    let serverId = req.body.serverId;
    let providerAgentIds = req.body.providerAgentIds;
    let status = req.body.status;

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }

        let sqlVal = [companyNickname, username];
        let sqlUpd = 'UPDATE Company SET companyNickname = ?, lastEditUser = ?, lastEditTime = utc_timestamp()'; 
        
        if(!utils.checkIfEmpty(whiteip)) {
            sqlUpd += ', whiteip = ?';
            sqlVal.push(whiteip);
        }

        if (providerAgentIds) {
            sqlUpd += ', providerAgentIds = ?';
            sqlVal.push(providerAgentIds);
        }

        if (!utils.checkIfEmpty(status)) {
            if(!utils.checkIfAllNumbers(status)) {

                await conn.rollback();

                return res.status(200).json(
                    utils.respObj(1051, "PARAM_TYPE_ERROR", "status")
                );
            }

            if(!utils.checkStrLen(status, 1, 5)) {

                await conn.rollback();

                return res.status(200).json(
                    utils.respObj(1054, "PARAM_LEN_LIMIT", "status")
                );
            }

            sqlUpd += ', status = ?';
            sqlVal.push(status);
        }


        if (!utils.checkIfEmpty(serverId)) {
            if(!utils.checkIfAllNumbers(serverId)) {

                await conn.rollback();
                
                return res.status(200).json(
                    utils.respObj(1051, "PARAM_TYPE_ERROR", "serverId")
                );
            }

            if(!utils.checkStrLen(serverId, 1, 15)) {

                await conn.rollback();

                return res.status(200).json(
                    utils.respObj(1054, "PARAM_LEN_LIMIT", "serverId")
                );
            }

            sqlUpd += ', serverId = ?';
            sqlVal.push(serverId);
        }

        sqlUpd += ' WHERE companyId = ? AND isDelete = 0;';
        sqlVal.push(companyId);

        await conn.execute(sqlUpd, sqlVal);

        let sqlSel = "SELECT cp.id, cp.companyId, cp.serverId, cp.companyNickname, cp.providerAgentIds, cp.desKey, cp.md5Key, cp.whiteip, cp.status, cp.createUser, DATE_FORMAT(cp.createTime, '%Y-%m-%d %T') AS createTime, cp.lastEditUser, DATE_FORMAT(cp.lastEditTime, '%Y-%m-%d %T') AS lastEditTime " +
                     'FROM Company cp WHERE cp.companyId = ? AND isDelete = 0;';
        
        let sqlSelVal = [companyId];

        let row = await conn.execute(sqlSel, sqlSelVal);
        
        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
        await conn.execute(sl.sqlSyslog, sqlLogVal);

        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, row[0][0])
        );   

    } catch(err) {
        console.log(err);

        await conn.rollback();
        
        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );

    } finally {
        console.log('modifyCompany release');

        conn.release();
    }

}


//  platform/company/{companyId}
async function delCompany(req, res) {

    let companyId = req.params.companyId;

    if(utils.checkIfEmpty(companyId)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "companyId")
        );
    }
    
    if(!utils.checkIfAllNumbers(companyId)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "companyId")
        );
    }

    if(!utils.checkStrLen(companyId, 1, 15)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "companyId")
        );
    }

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }
        
        let sqlDel = 'DELETE FROM Company WHERE companyId = ?;';
        let sqlVal = [companyId];

        await conn.execute(sqlDel, sqlVal);


        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
        await conn.execute(sl.sqlSyslog, sqlLogVal);

        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, null)
        );   

    } catch(err) {
        console.log(err);

        await conn.rollback();

        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );
    
    } finally {
        console.log('delCompany release');

        conn.release();
    }

}

module.exports = {
    getCompany,
    addCompany,
    modifyCompany,
    delCompany
}