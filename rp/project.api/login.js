
const db = require('../project.modules/mysql_instance');
const cache = require('../project.modules/redis_instance');
const utils = require('../project.utils/common');
const cfg = require('../project.config/cfg');
const totp = require('otplib');

var crypto = require('crypto');

async function test(req, res) {

    let a = req.body.aaa;
    
    return res.status(200).json(
        utils.respObj(0, "", utils.checkStrLen(a, 1, 2))
    );
}

//  platform/auth/totp
async function verifyTOTP(req, res) {
    
    const authHeader = req.headers.authorization
    if (!authHeader) {
        return res.status(401).json(
            utils.respObj(1008, "TOKEN_MISS", null)
        );
    }

    const totpToken = req.body.totpToken;

    if (!totpToken) {
        return res.status(401).json(
            utils.respObj(1009, "TOTP_TOKEN_MISS", null)
        );
    }

    let conn = await db.getConnection();
    // await conn.beginTransaction();
    try {
        let jsn = await cache.redisClient.get(authHeader);
        if(!jsn) {
            return res.status(200).json(
                utils.respObj(1002, "TOKEN_EXPIRED", null)
            );
        }

        let obj = JSON.parse(jsn);
        let secretKey = obj.secretKey;
        
        let chk = totp.authenticator.check(totpToken, secretKey);
        if(!chk) {
            return res.status(200).json(
                utils.respObj(1010, "TOTP_VERIFY_DENY", null)
            );
        }

        if (req.socket.remoteAddress) {
            await conn.execute("update RP_User set lastloginIP = ?, lastloginTime = utc_timestamp() where name = ?", [req.socket.remoteAddress, obj.name]);
        }
        
        // let sqlSel = 'select id, parentId, level from RP_User where name = ? and status = 0 and isDelete = 0;'
        let sqlSel = 'select ru.id, ru.name, ru.nickName, ru.parentId, ru.level, rr.top from RP_User ru ' +
                     'inner join RP_Role rr on ru.roleId = rr.id ' +
                     'where ru.name = ? and ru.status = 0 and ru.isDelete = 0;';

        let rows = await conn.execute(sqlSel, [obj.name]);
        if (rows[0].length <= 0) {
            return res.status(200).json(
                utils.respObj(5900, "USER_NOT_FOUND", null)
            );    
        }
        
        obj.id = rows[0][0].id;
        obj.name = rows[0][0].name;
        obj.nickName = rows[0][0].nickName;
        obj.parentId = rows[0][0].parentId;
        obj.level = rows[0][0].level;
        obj.top = rows[0][0].top;
        obj.pass = true;
        await cache.redisClient.set(authHeader, JSON.stringify(obj), {
            EX: cfg.cfg.token_expire
        });

    } catch(err) {
        console.log(err);

        // await conn.rollback();

        return res.status(500).json(
            utils.respObj(-1, "SYS_ERROR", null)
        );
    } finally {
        console.log('verifyTOTP release');

        conn.release();
    }
    
    return res.status(200).json(
        utils.respObj(0, null, null)
    );
}

//  platform/auth
async function login(req, res) {
    
    let name = req.body.name;
    let password = req.body.password;

    if (!name || !password) {
        return res.status(401).json(
            utils.respObj(1000, "USER_OR_PASSWORD_INCORRECT", null)
        );
    }

    let token = '';
    let roleId;
    let top;
    let conn = await db.getConnection();
    // await conn.beginTransaction();
    try {
        // let rows = await conn.execute("select password, secretKey, roleId from RP_User where name = ? and status = 0 and isDelete = 0;", [name]);
        let rows = await conn.execute("select ru.password, ru.secretKey, ru.roleId, rr.top from RP_User ru inner join RP_Role rr on ru.roleId = rr.id where ru.name = ? and ru.status = 0 and ru.isDelete = 0;", [name]);
        if (rows[0].length <= 0) {
            return res.status(200).json(
                utils.respObj(5900, "USER_NOT_FOUND", null)
            );    
        }

        let cryPwd = crypto.createHash('sha256').update(password).digest('hex');
        if(cryPwd !== rows[0][0].password) {
            return res.status(200).json(
                utils.respObj(1000, "USER_OR_PASSWORD_INCORRECT", null)
            );
        }
        
        // uuid = crypto.randomUUID();
        token = crypto.createHash('sha256').update(name + '.' + cfg.cfg.server_key + '.' + Math.floor(Date.now() / 1000)).digest('hex');

        let tObj = new Object();
        tObj.name = name;
        tObj.secretKey = rows[0][0].secretKey;
        tObj.roleId = rows[0][0].roleId;
        tObj.pass = false;

        roleId = rows[0][0].roleId;
        top = rows[0][0].top;

        await cache.redisClient.set(token, JSON.stringify(tObj), {
            EX: cfg.cfg.token_expire
        });

    } catch(err) {
        console.error(err);
        return res.status(500).json(
            utils.respObj(-1, "SYS_ERROR", null)
        );
    } finally {
        console.log('login release');

        conn.release();
    }

    let rtnObj = new Object();
    rtnObj.name = name;
    rtnObj.roleId = roleId;
    rtnObj.top = top;
    rtnObj.token = token;

    return res.status(200).json(
        utils.respObj(0, null, rtnObj)
    );

}

module.exports = {
    test,
    login,
    verifyTOTP
}