const db = require('../project.modules/mysql_instance');
const sl = require('../project.api/syslog');
const utils = require('../project.utils/common');

//  platform/game
async function getGame(req, res) {

    let page = req.query.page;
    let pageSize = req.query.pageSize;

    if(!page || !pageSize) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", null)
        );
    }

    let nPage = parseInt(page);
    let nPageSize = parseInt(pageSize);
    let start = nPage * nPageSize;
    if (isNaN(start)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", null)
        );
    }

    let gameId = req.query.gameId;
    let gameName = req.query.gameName;
    let status = req.query.status;

    let conn = await db.getConnection();
    try {
        let condition = '';
        let conditionVal = [];
        let obj = new Object();
        obj.count = 0;
        obj.rows = [];

        if(!utils.checkIfEmpty(status)) {
            if(!utils.checkIfAllNumbers(status)) {
                obj.illegalParam = "status";
                return res.status(200).json(
                    utils.respObj(1051, "PARAM_TYPE_ERROR", obj)
                );
            }

            condition += ' AND gi.status = ? ';
            conditionVal.push(status);
        }

        if(!utils.checkIfEmpty(gameId)) {
            if(!utils.checkIfAllNumbers(gameId)) {
                obj.illegalParam = "gameId";
                return res.status(200).json(
                    utils.respObj(1051, "PARAM_TYPE_ERROR", obj)
                );
            }

            if(!utils.checkStrLen(gameId, 1, 10)) {
                obj.lengthLimitParam = "gameId";
                return res.status(200).json(
                    utils.respObj(1054, "PARAM_LEN_LIMIT", obj)
                );
            }

            condition += ' AND gi.gameId = ?';
            conditionVal.push(gameId);
        }

        if (!utils.checkIfEmpty(gameName)) {
            if (utils.checkSpecialCharacters(gameName)) {
                obj.illegalParam = "gameName";
                return res.status(200).json(
                    utils.respObj(1053, "ILLEGAL_CHARACTER", obj)
                );
            }

            condition += " AND gi.gameName LIKE '%" + gameName + "%'";
        }


        let sqlSelCnt = "SELECT count(*) AS cnt FROM GameInfo gi WHERE gi.isDelete = 0 " + condition +"; ";
        
        let rowCnt = await conn.execute(sqlSelCnt, conditionVal);

        let sqlSel = 'SELECT gi.id, gi.gameId, gi.gameName, gi.providerId, gi.providerGameId, gi.supportCurrency, gi.supportLanguage, gi.status, gi.createUser, gi.createTime, gi.lastEditUser, gi.lastEditTime ' + 
                     'FROM GameInfo gi WHERE gi.isDelete = 0' + condition + ' LIMIT ?, ?;'
        
        conditionVal.push(start.toString());
        conditionVal.push(pageSize.toString());

        let rows = await conn.execute(sqlSel, conditionVal);

        // let obj = new Object();
        obj.count = rowCnt[0][0].cnt;
        obj.rows = rows[0];

        return res.status(200).json(
            utils.respObj(0, null, obj)
        );
        
    } catch(err) {
        console.log(err);

        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );

    } finally {
        console.log('getGame release');

        conn.release();
    }

}

//  platform/game
async function addGame(req, res) {

    let gameId = req.body.gameId;
    let gameName = req.body.gameName;
    let providerId = req.body.providerId;
    let providerGameId = req.body.providerGameId;
    let supportCurrency = req.body.supportCurrency;
    let supportLanguage = req.body.supportLanguage;

    if(utils.checkIfEmpty(gameId)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "gameId")
        );
    }

    if(!utils.checkIfAllNumbers(gameId)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "gameId")
        );
    }

    if(!utils.checkStrLen(gameId, 1, 8)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "gameId")
        );
    }

    if(utils.checkIfEmpty(providerId)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "providerId")
        );
    }

    if(!utils.checkIfAllNumbers(providerId)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "providerId")
        );
    }

    if(!utils.checkStrLen(providerId, 1, 15)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "providerId")
        );
    }

    if(utils.checkIfEmpty(providerGameId)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "providerGameId")
        );
    }

    if(!utils.checkIfAllNumbers(providerGameId)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "providerGameId")
        );
    } 

    if(!utils.checkStrLen(providerGameId, 1, 8)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "providerGameId")
        );
    }

    if(utils.checkIfEmpty(gameName)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "gameName")
        );
    }

    if (utils.checkSpecialCharacters(gameName)) {
        return res.status(200).json(
            utils.respObj(1053, "ILLEGAL_CHARACTER", "gameName")
        );
    }

    if(utils.checkIfEmpty(supportCurrency)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "supportCurrency")
        );
    }

    if (utils.checkSpecialCharacters(supportCurrency)) {
        return res.status(200).json(
            utils.respObj(1053, "ILLEGAL_CHARACTER", "supportCurrency")
        );
    }

    if(utils.checkIfEmpty(supportLanguage)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "supportLanguage")
        );
    }

    if (utils.checkSpecialCharacters(supportLanguage)) {
        return res.status(200).json(
            utils.respObj(1053, "ILLEGAL_CHARACTER", "supportLanguage")
        );
    }

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        
        let sqlCnt = 'SELECT count(*) AS cnt FROM GameInfo pvd WHERE pvd.gameId = ?;';
        let rowCnt = await conn.execute(sqlCnt, [gameId]);
        if (rowCnt[0][0].cnt !== 0) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(1108, 'GAME_ID_EXIST', gameId)
            );
        }

        let sqlProviderCnt = 'SELECT count(*) AS cnt FROM Provider p WHERE p.providerId = ?';
        let rowPvdCnt = await conn.execute(sqlProviderCnt, [providerId]);
        if (rowPvdCnt[0][0].cnt === 0) {

            await conn.rollback();

            let obj = new Object();
            obj.gameId = gameId;
            obj.providerId = providerId;

            return res.status(200).json(
                utils.respObj(1109, 'PROVIDER_NO_FOUND', obj)
            );
        }
    
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }

        let sqlInsert = 'INSERT INTO GameInfo' + 
                        '(gameId, gameName, providerId, providerGameId, supportCurrency, supportLanguage, createUser, createTime, lastEditUser, lastEditTime, isDelete)VALUES' +
                        '(?, ?, ?, ?, ?, ?, ?, utc_timestamp(), ?, utc_timestamp(), ?);'
    
        let sqlVal = [gameId, gameName, providerId, providerGameId, supportCurrency, supportLanguage, username, username, 0];
    
        let resp = await conn.execute(sqlInsert, sqlVal);
    
        let id = resp[0].insertId;
    
        let sqlSel = "SELECT gi.id, gi.gameId, gi.gameName, gi.providerId, gi.providerGameId, gi.supportCurrency, gi.supportLanguage, gi.status, gi.createUser, DATE_FORMAT(gi.createTime, '%Y-%m-%d %T') AS createTime, gi.lastEditUser, DATE_FORMAT(gi.lastEditTime, '%Y-%m-%d %T') AS lastEditTime " + 
                     'FROM GameInfo gi WHERE gi.id = ?;';
    
        let row = await conn.execute(sqlSel, [id.toString()]);
         
        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
    
        await conn.execute(sl.sqlSyslog, sqlLogVal);
    
        await conn.commit();
    
        return res.status(200).json(
            utils.respObj(0, null, row[0][0])
        );

    } catch(err) {
        console.log(err);

        await conn.rollback();
        
        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );

    } finally {
        console.log('addGame release');

        conn.release();
    }
}

//  platform/game
async function modifyGame(req, res) {

    let gameId = req.body.gameId;
    let gameName = req.body.gameName;
    let providerId = req.body.providerId;
    let providerGameId = req.body.providerGameId;
    let supportCurrency = req.body.supportCurrency;
    let supportLanguage = req.body.supportLanguage;
    let status = req.body.status;

    if(utils.checkIfEmpty(gameId)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "gameId")
        );
    }

    if(!utils.checkIfAllNumbers(gameId)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "gameId")
        );
    }

    if(!utils.checkStrLen(gameId, 1, 8)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "gameId")
        );
    }

    if(utils.checkIfEmpty(providerId)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "providerId")
        );
    }

    if(!utils.checkIfAllNumbers(providerId)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "providerId")
        );
    }

    if(!utils.checkStrLen(providerId, 1, 15)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "providerId")
        );
    }

    if(utils.checkIfEmpty(providerGameId)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "providerGameId")
        );
    }

    if(!utils.checkIfAllNumbers(providerGameId)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "providerGameId")
        );
    } 

    if(!utils.checkStrLen(providerGameId, 1, 8)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "providerGameId")
        );
    }

    if(utils.checkIfEmpty(gameName)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "gameName")
        );
    }

    if (utils.checkSpecialCharacters(gameName)) {
        return res.status(200).json(
            utils.respObj(1053, "ILLEGAL_CHARACTER", "gameName")
        );
    }

    if(utils.checkIfEmpty(supportCurrency)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "supportCurrency")
        );
    }

    if (utils.checkSpecialCharacters(supportCurrency)) {
        return res.status(200).json(
            utils.respObj(1053, "ILLEGAL_CHARACTER", "supportCurrency")
        );
    }

    if(utils.checkIfEmpty(supportLanguage)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "supportLanguage")
        );
    }

    if (utils.checkSpecialCharacters(supportLanguage)) {
        return res.status(200).json(
            utils.respObj(1053, "ILLEGAL_CHARACTER", "supportLanguage")
        );
    }

    if(utils.checkIfEmpty(status)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "status")
        );
    }

    if(!utils.checkIfAllNumbers(status)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "status")
        );
    }

    if(!utils.checkStrLen(status, 1, 5)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "status")
        );
    }

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }

        let sqlProviderCnt = 'SELECT count(*) AS cnt FROM Provider p WHERE p.providerId = ?';
        let rowPvdCnt = await conn.execute(sqlProviderCnt, [providerId]);
        if (rowPvdCnt[0][0].cnt === 0) {

            await conn.rollback();

            let obj = new Object();
            obj.gameId = gameId;
            obj.providerId = providerId;
            return res.status(200).json(
                utils.respObj(1109, 'PROVIDER_NO_FOUND', obj)
            );
        }

        let sqlVal = [gameName, username, status, providerId, providerGameId, supportCurrency, supportLanguage];
        let sqlUpd = 'UPDATE GameInfo ' + 
                     'SET gameName = ?, lastEditUser = ?, lastEditTime = utc_timestamp()' + 
                     ', status = ?, providerId = ?, providerGameId = ?, supportCurrency = ?, supportLanguage = ?'

        sqlUpd += ' WHERE gameId = ?;';
        sqlVal.push(gameId);

        await conn.execute(sqlUpd, sqlVal);

        let sqlSel = "SELECT gi.id, gi.gameId, gi.gameName, gi.providerId, gi.providerGameId, gi.supportCurrency, gi.supportLanguage, gi.status, gi.createUser, DATE_FORMAT(gi.createTime, '%Y-%m-%d %T') AS createTime, gi.lastEditUser, DATE_FORMAT(gi.lastEditTime, '%Y-%m-%d %T') AS lastEditTime " + 
                     'FROM GameInfo gi WHERE gi.gameId = ?;';

        let row = await conn.execute(sqlSel, [gameId.toString()]);

        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
        await conn.execute(sl.sqlSyslog, sqlLogVal);

        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, row[0][0])
        );

    } catch(err) {
        console.log(err);

        await conn.rollback();
        
        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );

    } finally {
        console.log('modifyGame release');

        conn.release();
    }

}

//  platform/game
async function delGame(req, res) {

    let gameId = req.params.gameId;

    if(utils.checkIfEmpty(gameId)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "gameId")
        );
    }
    
    if(!utils.checkIfAllNumbers(gameId)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "gameId")
        );
    }

    if(!utils.checkStrLen(gameId, 1, 8)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "gameId")
        );
    }

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }

        let sqlDel = 'DELETE FROM GameInfo WHERE gameId = ?;';
        let sqlVal = [gameId.toString()];

        await conn.execute(sqlDel, sqlVal);
        
        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
        await conn.execute(sl.sqlSyslog, sqlLogVal);

        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, null)
        );

    } catch(err) {
        console.log(err);

        await conn.rollback();

        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );

    } finally {
        console.log('delGame release');

        conn.release();
    }

}

module.exports = {
    getGame, 
    addGame,
    modifyGame,
    delGame
}