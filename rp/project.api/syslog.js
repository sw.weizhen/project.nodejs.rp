const db = require('../project.modules/mysql_instance');
const utils = require('../project.utils/common');

const sqlSyslog =   'INSERT INTO Sys_Log(`action`, mark, `user`, userip, actionTime, userLog)' +
                    'VALUES(?, ?, ?, ?, utc_timestamp(), ?);';

async function getSyslog(req, res) {
    
    let page = req.query.page;
    let pageSize = req.query.pageSize;
    if(!page || !pageSize) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", null)
        );
    }

    let nPage = parseInt(page);
    let nPageSize = parseInt(pageSize);
    let start = nPage * nPageSize;
    if (isNaN(start)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", null)
        );
    }

    let startTime = req.query.startTime;
    let endTime = req.query.endTime;
    let action = req.query.action;
    let mark = req.query.mark;
    let user = req.query.user;

    let conn = await db.getConnection();
    try {
        let condition = '';
        let conditionVal = [];
        if (user) {
            condition += 'AND user = ? ';
            conditionVal.push(user);
        }

        if (mark) {
            condition += 'AND mark = ? ';
            conditionVal.push(mark);
        }

        if (action) {
            condition += ("AND action LIKE '%" + action + "%' ");
        }

        if (startTime) {
            condition += "AND actionTime >= '" + utils.convertTimestamp(startTime) + "'";
        }

        if (endTime) {
            condition += "AND actionTime <= '" + utils.convertTimestamp(endTime) + "'";
        }

        if (condition.length > 0) {
            condition = 'WHERE ' + condition.slice(3, condition.length);
        }

        let cntSql = 'SELECT count(*) AS cnt FROM Sys_Log ' + condition + ';';

        let rowCnt = await conn.execute(cntSql, conditionVal);

        let sqlSel = "SELECT id, `action`, mark, `user`, userip, DATE_FORMAT(actionTime, '%Y-%m-%d %T') AS actionTime, userLog FROM Sys_Log " +
                     condition + ' ORDER BY actionTime DESC LIMIT ?, ?;';
        
        conditionVal.push(start.toString());
        conditionVal.push(pageSize.toString());

        let rows = await conn.execute(sqlSel, conditionVal);
        let obj = new Object();
        obj.count = rowCnt[0][0].cnt;
        obj.rows = rows[0];

        return res.status(200).json(
            utils.respObj(0, null, obj)
        );

    } catch(err) {
        console.log(err);
        
        await syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );

    } finally {
        console.log('getSyslog release');

        conn.release();
    }
}

async function syserrlog(req, res, err) {

    let objData = new Object();
    objData.action = res.req.url;
    objData.mark = res.req.method;
    objData.user = req.userObj.name;
    objData.userip = req.socket.remoteAddress;

    let action = objData.action;
    if (!action) {
        action = 'unknow';
    }

    let mark = objData.mark;
    if (!mark) {
        mark = 'unknow';
    }

    let user = objData.user;
    if (!user) {
        user = 'unknow';
    }

    let userip = objData.userip;
    if (!userip) {
        userip = 'unknow';
    }

    if (!err) {
        err = 'unknow';
    }
   
    let conn = await db.getConnection();
    // await conn.beginTransaction();
    try {
   
        let sqlVal = [action, mark, user, userip, err];
        
        await conn.execute(sqlSyslog, sqlVal);
   
    } catch(err) {
        console.log(err);
    
    } finally {
        console.log('syserrlog release');
    
        conn.release();
    }
}

module.exports = {
    sqlSyslog,
    getSyslog,
    syserrlog
}