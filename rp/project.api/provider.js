const db = require('../project.modules/mysql_instance');
const sl = require('../project.api/syslog');
const utils = require('../project.utils/common');

async function getProvider(req, res) {

    let page = req.query.page;
    let pageSize = req.query.pageSize;
    let providerId = req.query.providerId;
    let providerNickname = req.query.providerNickname;
    let status = req.query.status;
    if(!page || !pageSize) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", null)
        );
    }

    let nPage = parseInt(page);
    let nPageSize = parseInt(pageSize);
    let start = nPage * nPageSize;
    if (isNaN(start)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", null)
        );
    }
    
    let conn = await db.getConnection();
    try {
        let condition = '';
        let conditionVal = [];
        let obj = new Object();
        obj.count = 0;
        obj.rows = [];

        if(!utils.checkIfEmpty(providerId)) {
            if(!utils.checkIfAllNumbers(providerId)) {
                obj.illegalParam = "providerId";
                return res.status(200).json(
                    utils.respObj(1051, "PARAM_TYPE_ERROR", obj)
                );
            }

            if(!utils.checkStrLen(providerId, 1, 15)) {
                obj.lengthLimitParam = "providerId";
                return res.status(200).json(
                    utils.respObj(1054, "PARAM_LEN_LIMIT", obj)
                );
            }

            condition += 'AND pvd.providerId = ? ';
            conditionVal.push(providerId);
        }

        if(!utils.checkIfEmpty(status)) {
            if(!utils.checkIfAllNumbers(status)) {
                obj.illegalParam = "status";
                return res.status(200).json(
                    utils.respObj(1051, "PARAM_TYPE_ERROR", obj)
                );
            }

            if(!utils.checkStrLen(status, 1, 5)) {
                obj.lengthLimitParam = "status";
                return res.status(200).json(
                    utils.respObj(1054, "PARAM_LEN_LIMIT", obj)
                );
            }

            condition += 'AND pvd.status = ? ';
            conditionVal.push(status);
        }


        if (!utils.checkIfEmpty(providerNickname)) {
            if (utils.checkSpecialCharacters(providerNickname)) {
                obj.illegalParam = "providerNickname";
                return res.status(200).json(
                    utils.respObj(1053, "ILLEGAL_CHARACTER", obj)
                );
            }

            condition += "AND pvd.providerNickname LIKE '%" + providerNickname + "%'" ;
        }

        let sqlCnt = 'SELECT count(*) AS cnt FROM Provider pvd WHERE pvd.isDelete = 0 ' + condition + ';';

        let rowCnt = await conn.execute(sqlCnt, conditionVal);

        let sqlSel = "SELECT pvd.id, pvd.providerId, pvd.providerNickname, pvd.gameApiUrl, pvd.desKey, pvd.md5Key, pvd.whiteip, pvd.status, pvd.createUser, DATE_FORMAT(pvd.createTime, '%Y-%m-%d %T') AS createTime, pvd.lastEditUser, DATE_FORMAT(pvd.lastEditTime, '%Y-%m-%d %T') AS lastEditTime " + 
                     'FROM Provider pvd WHERE pvd.isDelete = 0 ' + condition + ' LIMIT ?, ?;';
        
        conditionVal.push(start.toString());
        conditionVal.push(pageSize.toString());             
     
        let rows = await conn.execute(sqlSel, conditionVal);
        
        obj.count = rowCnt[0][0].cnt;
        obj.rows = rows[0];

        return res.status(200).json(
            utils.respObj(0, null, obj)
        );
         
    } catch(err) {
        console.log(err);

        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );

    } finally {
        console.log('getProvider release');

        conn.release();
    }
}

async function addProvider(req, res) {

    let providerId = req.body.providerId;
    let providerNickname = req.body.providerNickname;
    if(utils.checkIfEmpty(providerId)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "providerId")
        );
    }

    if(!utils.checkIfAllNumbers(providerId)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "providerId")
        );
    }

    if(!utils.checkStrLen(providerId, 1, 15)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "providerId")
        );
    }

    if(utils.checkIfEmpty(providerNickname)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "providerNickname")
        );
    }

    if (utils.checkSpecialCharacters(providerNickname)) {
        return res.status(200).json(
            utils.respObj(1053, "ILLEGAL_CHARACTER", "providerNickname")
        );
    }

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {

        let sqlCnt = 'SELECT count(*) AS cnt FROM Provider pvd WHERE pvd.providerId = ?;';
        let rowCnt = await conn.execute(sqlCnt, [providerId]);
        
        if (rowCnt[0][0].cnt !== 0) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(1106, 'PROVIDER_EXIST', providerId)
            );  

        }

        
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }

        let desKey = utils.getRandomnum(16);
        let md5Key = utils.getRandomnum(16);

        let sqlInsert = "INSERT INTO Provider(providerId, providerNickname, gameApiUrl, desKey, md5Key, whiteip, status, createUser, createTime, lastEditUser, lastEditTime, isDelete)" + 
                        "VALUES(?, ?, '', ?, ?, '', 0, ?, utc_timestamp(), ?, utc_timestamp(), 0);";

        let sqlVal = [providerId, providerNickname, desKey, md5Key, username, username];

        let resp = await conn.execute(sqlInsert, sqlVal);

        let id = resp[0].insertId;

        let sqlSel = "SELECT pvd.id, pvd.providerId, pvd.providerNickname, pvd.gameApiUrl, pvd.desKey, pvd.md5Key, pvd.whiteip, pvd.status, pvd.createUser, DATE_FORMAT(pvd.createTime, '%Y-%m-%d %T') AS createTime, pvd.lastEditUser, DATE_FORMAT(pvd.lastEditTime, '%Y-%m-%d %T') AS lastEditTime " + 
                     'FROM Provider pvd WHERE pvd.id = ?;';

        let row = await conn.execute(sqlSel, [id]);
        
        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];

        await conn.execute(sl.sqlSyslog, sqlLogVal);

        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, row[0][0])
        );
             
    } catch(err) {
        console.log(err);

        await conn.rollback();
        
        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );
    
    } finally {
        console.log('addProvider release');

        conn.release();
    }
}

async function modifyProvider(req, res) {

    let providerId = req.body.providerId;
    let providerNickname = req.body.providerNickname;
    let whiteip = req.body.whiteip;
    
    if(utils.checkIfEmpty(providerId)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "providerId")
        );
    }
    
    if(!utils.checkIfAllNumbers(providerId)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "providerId")
        );
    }

    if(!utils.checkStrLen(providerId, 1, 15)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "providerId")
        );
    }

    if(utils.checkIfEmpty(providerNickname)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "providerNickname")
        );
    }

    if (utils.checkSpecialCharacters(providerNickname)) {
        return res.status(200).json(
            utils.respObj(1053, "ILLEGAL_CHARACTER", "providerNickname")
        );
    }

    let status = req.body.status;
    let gameApiUrl = req.body.gameApiUrl;

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }

        let sqlVal = [providerNickname, username];
        let sqlUpd = "UPDATE Provider " + 
                     "SET providerNickname = ?, lastEditUser = ?, lastEditTime = utc_timestamp() "

        if(!utils.checkIfEmpty(whiteip)) {
            sqlUpd += ', whiteip = ?';
            sqlVal.push(whiteip);
        }             

        if(!utils.checkIfEmpty(gameApiUrl)) {
            sqlUpd += ', gameApiUrl = ?';
            sqlVal.push(gameApiUrl);
        }

        if(!utils.checkIfEmpty(status)) {
            if(!utils.checkIfAllNumbers(status)) {

                await conn.rollback();

                return res.status(200).json(
                    utils.respObj(1051, "PARAM_TYPE_ERROR", "status")
                );
            }

            if(!utils.checkStrLen(status, 1, 5)) {

                await conn.rollback();

                return res.status(200).json(
                    utils.respObj(1054, "PARAM_LEN_LIMIT", "status")
                );
            }

            sqlUpd += ', status = ?';
            sqlVal.push(status);
        }

        sqlUpd += " WHERE providerId = ?;";
        sqlVal.push(providerId);
        
        await conn.execute(sqlUpd, sqlVal);

        let sqlSel = "SELECT pvd.id, pvd.providerId, pvd.providerNickname, pvd.gameApiUrl, pvd.desKey, pvd.md5Key, pvd.whiteip, pvd.status, pvd.createUser, DATE_FORMAT(pvd.createTime, '%Y-%m-%d %T') AS createTime, pvd.lastEditUser, DATE_FORMAT(pvd.lastEditTime, '%Y-%m-%d %T') AS lastEditTime " + 
                     'FROM Provider pvd WHERE pvd.providerId = ? AND pvd.isDelete = 0;';

        let row = await conn.execute(sqlSel, [providerId]);
        
        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
        await conn.execute(sl.sqlSyslog, sqlLogVal);

        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, row[0][0])
        );  


    } catch(err) {
        console.log(err);

        await conn.rollback();
        
        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );
    
    } finally {
        console.log('modifyProvider release');

        conn.release();
    }
}

async function delProvider(req, res) {

    let providerId = req.params.providerId;

    if(utils.checkIfEmpty(providerId)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "providerId")
        );
    }
    
    if(!utils.checkIfAllNumbers(providerId)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "providerId")
        );
    }

    if(!utils.checkStrLen(providerId, 1, 15)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "providerId")
        );
    }

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }

        let sqlDel = 'DELETE FROM Provider WHERE providerId = ?;';
        let sqlVal = [providerId];
        await conn.execute(sqlDel, sqlVal);

        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
        await conn.execute(sl.sqlSyslog, sqlLogVal);

        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, null)
        );

    } catch(err) {
        console.log(err);

        await conn.rollback();

        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );
    
    } finally {
        console.log('delProvider release');

        conn.release();
    } 
}

module.exports = {
    getProvider,
    addProvider,
    modifyProvider,
    delProvider
}