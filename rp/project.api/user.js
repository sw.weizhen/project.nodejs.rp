const db = require('../project.modules/mysql_instance');
const sl = require('../project.api/syslog');
const utils = require('../project.utils/common');
const totp = require('otplib');


//  platform/user
async function getUser(req, res) {

    let page = req.query.page;
    let pageSize = req.query.pageSize;
    let name = req.query.name;

    if(!page || !pageSize) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", null)
        );
    }

    let nPage = parseInt(page);
    let nPageSize = parseInt(pageSize);
    let start = nPage * nPageSize;

    if (isNaN(start)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", null)
        );
    }


    if(!utils.checkIfEmpty(name)) {
        let obj = new Object()
        obj.count = 0;
        obj.rows = [];
        obj.illegalParam = "name";

        if(utils.checkSpecialCharacters(name)) {
            return res.status(200).json(
                utils.respObj(1053, "ILLEGAL_CHARACTER", obj)
            );
        }
    }

    let conn = await db.getConnection();
    try {
        let selCurTop = 'select rr.top from RP_User ru ' +
                        'inner join RP_Role rr on ru.roleId = rr.id ' +
                        'where ru.name = ? and ru.status = 0 and ru.isDelete = 0;';
        let selCurVal = [req.userObj.name];
        let rowCurTop = await conn.execute(selCurTop, selCurVal);
        if (rowCurTop[0].length <= 0) {
            return res.status(200).json(
                utils.respObj(5900, "USER_NOT_FOUND", null)
            );    
        }

        let userTop = rowCurTop[0][0].top;

        let cntSql = 'SELECT count(*) AS cnt FROM RP_User ru INNER JOIN RP_Role rr on ru.roleId = rr.id ';
        cntSql += "WHERE rr.top > ? AND ru.isDelete = 0 ";
        if (name) {
            cntSql += "AND ru.name LIKE '%" + name + "%'" ;
        }
        cntSql += ";";


        let rowCnt = await conn.execute(cntSql, [userTop.toString()]);

        let sqlSel = "SELECT ru.id, ru.name, ru.nickname, ru.parentId, ru.level, ru.secretKey, ru.roleId, ru.status, DATE_FORMAT(ru.lastloginTime, '%Y-%m-%d %T') AS lastloginTime, DATE_FORMAT(ru.lastEditTime, '%Y-%m-%d %T') AS lastEditTime, ru.createUser, ru.lastEditUser " +
                     'FROM RP_User ru INNER JOIN RP_Role rr on ru.roleId = rr.id ';
        
        sqlSel += "WHERE rr.top > ? AND ru.isDelete = 0 ";
        if (!utils.checkIfEmpty(name)) {
            sqlSel += "AND ru.name LIKE '%" + name + "%' " ;
        }
        sqlSel += 'LIMIT ?, ?;';

        let rows = await conn.execute(sqlSel, [userTop.toString(), start.toString(), pageSize.toString()]);

        let obj = new Object();
        obj.count = rowCnt[0][0].cnt;
        obj.rows = rows[0];

        return res.status(200).json(
            utils.respObj(0, null, obj)
        );

    } catch(err) {
        console.log(err);

        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );

    } finally {
        console.log('getUser release');

        conn.release();
    }
    
}

//  platform/user
async function addUser(req, res) {

    let name = req.body.name;
    let roleId = req.body.roleId;
    let password = req.body.password;
    let nickname = req.body.nickname;
    
    if(utils.checkIfEmpty(name)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "name")
        );
    }

    if(utils.checkSpecialCharacters(name)) {
        return res.status(200).json(
            utils.respObj(1053, "ILLEGAL_CHARACTER", "name")
        );
    }

    if(utils.checkIfEmpty(roleId)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "roleId")
        );
    }
    
    if(!utils.checkIfAllNumbers(roleId)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "roleId")
        );
    }

    if(!utils.checkStrLen(roleId, 1, 18)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "roleId")
        );
    }

    if(utils.checkIfEmpty(password)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "password")
        );
    }

    if(utils.checkIfEmpty(nickname)) {
        nickname = '';
    }

    if (utils.checkSpecialCharacters(nickname)) {
        return res.status(200).json(
            utils.respObj(1053, "ILLEGAL_CHARACTER", 'nickname')
        );
    }

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        let sqlCnt = 'SELECT count(*) AS cnt FROM RP_User pvd WHERE pvd.name = ?;';
        let rowCnt = await conn.execute(sqlCnt, [name]);
        if (rowCnt[0][0].cnt !== 0) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(1107, 'USER_NAME_EXIST', name)
            );  
        }

        let username = req.userObj.name;
        let pid = req.userObj.id;
        let level = req.userObj.level;

        let selCurTop = 'select rr.top from RP_User ru ' +
                        'inner join RP_Role rr on ru.roleId = rr.id ' +
                        'where ru.name = ? and ru.status = 0 and ru.isDelete = 0;';
        let selCurVal = [username];
        let rowCurTop = await conn.execute(selCurTop, selCurVal);
        if (rowCurTop[0].length <= 0) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(5900, "USER_NOT_FOUND", null)
            );    
        }

        let selRoleTop = 'select top from RP_Role rr WHERE rr.id = ?;'
        let selRoleVal = [roleId.toString()];
        let rowRoleTop = await conn.execute(selRoleTop, selRoleVal);
        if (rowRoleTop[0].length <= 0) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(5504, "ROLE_NOT_FOUND", null)
            );    
        }

        let roleTop = rowRoleTop[0][0].top
        let curTop = rowCurTop[0][0].top;
        if (roleTop <= curTop) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(1052, 'PERMISSION_DENY', null)
            );
        }

        let secretKey = totp.authenticator.generateSecret();
        let cryptoPwd = utils.cryptoPwd(password);

        let sqlInsert = "INSERT INTO RP_User (name, nickname, parentId, level, password, secretKey, roleId, status, lastloginIP, createUser, createTime, lastEditUser, lastEditTime, isDelete)VALUES" + 
                        "(?, ?, ?, ?, ?, ?, ?, 0, '', ?, utc_timestamp(), ?, utc_timestamp(), 0);";
        
        let sqlVal = [name, nickname, pid, (level+1), cryptoPwd, secretKey, roleId, username, username];

        let resp = await conn.execute(sqlInsert, sqlVal);
        let id = resp[0].insertId;

        let sqlSel = "SELECT ru.id, ru.name, ru.nickname, parentId, level, ru.secretKey, ru.roleId, ru.status, DATE_FORMAT(ru.lastloginTime, '%Y-%m-%d %T') AS lastloginTime, DATE_FORMAT(ru.lastEditTime, '%Y-%m-%d %T') AS lastEditTime, ru.createUser, ru.lastEditUser " +
                     'FROM RP_User ru ' + 
                     'WHERE ru.id = ?;';
        
        let sqlSelVal = [id];

        let row = await conn.execute(sqlSel, sqlSelVal);
        
        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
        await conn.execute(sl.sqlSyslog, sqlLogVal);

        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, row[0][0])
        );   

    } catch(err) {
        console.log(err);

        await conn.rollback();
        
        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );
    
    } finally {
        console.log('addUser release');

        conn.release();
    }

}

//  platform/user
async function modifyUserProfile(req, res) {

    let id = req.body.id;
    let roleId = req.body.roleId;
    let status = req.body.status;
    let nickname = req.body.nickname;

    if(utils.checkIfEmpty(id)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "id")
        );
    }
    
    if(!utils.checkIfAllNumbers(id)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "id")
        );
    }

    if(utils.checkIfEmpty(roleId)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "roleId")
        );
    }
    
    if(!utils.checkIfAllNumbers(roleId)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "roleId")
        );
    }

    if(!utils.checkStrLen(roleId, 1, 18)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "roleId")
        );
    }

    if(utils.checkIfEmpty(status)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "status")
        );
    }
    
    if(!utils.checkIfAllNumbers(status)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "status")
        );
    }

    if(!utils.checkStrLen(status, 1, 5)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "status")
        );
    }

    if(utils.checkIfEmpty(nickname)) {
        nickname = '';
    }

    if (utils.checkSpecialCharacters(nickname)) {
        return res.status(200).json(
            utils.respObj(1053, "ILLEGAL_CHARACTER", 'nickname')
        );
    }

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        let username = req.userObj.name;
        if (!username) {
            throw 'inner data undefined';
        }

        if (req.userObj.id == id) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(1052, "PERMISSION_DENY", null)
            );
        }

        let selCurTop = 'select rr.top from RP_User ru ' +
                        'inner join RP_Role rr on ru.roleId = rr.id ' +
                        'where ru.name = ? and ru.status = 0 and ru.isDelete = 0;';
        let selCurVal = [username];
        let rowCurTop = await conn.execute(selCurTop, selCurVal);
        if (rowCurTop[0].length <= 0) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(5900, "USER_NOT_FOUND", null)
            );    
        }

        let selRoleTop = 'select top from RP_Role rr WHERE rr.id = ?;'
        let selRoleVal = [roleId.toString()];
        let rowRoleTop = await conn.execute(selRoleTop, selRoleVal);
        if (rowRoleTop[0].length <= 0) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(5504, "ROLE_NOT_FOUND", null)
            );    
        }

        let roleTop = rowRoleTop[0][0].top
        let curTop = rowCurTop[0][0].top;
        if (roleTop <= curTop) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(1052, 'PERMISSION_DENY', null)
            );
        }

        let sqlUpd = 'UPDATE RP_User ' + 
                     'SET nickname = ?, roleId = ?, status = ?, lastEditUser = ?, lastEditTime = utc_timestamp() ' + 
                     'WHERE id = ?;';

        let sqlVal = [nickname, roleId, status, username, id];

        await conn.execute(sqlUpd, sqlVal);

        let sqlSel = "SELECT ru.id, ru.name, ru.nickname, ru.secretKey, ru.roleId, ru.status, DATE_FORMAT(ru.lastloginTime, '%Y-%m-%d %T') AS lastloginTime, DATE_FORMAT(ru.lastEditTime, '%Y-%m-%d %T') AS lastEditTime, ru.createUser, ru.lastEditUser " +
                     'FROM RP_User ru ' + 
                     'WHERE ru.id = ?;';

        let sqlSelVal = [id];

        let row = await conn.execute(sqlSel, sqlSelVal);

        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
        await conn.execute(sl.sqlSyslog, sqlLogVal);

        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, row[0][0])
        );  

    } catch(err) {
        console.log(err);

        await conn.rollback();
        
        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );

    } finally {
        console.log('modifyUserProfile release');

        conn.release();
    }

}

//  platform/user/totp
async function modifyUserTOTP(req, res) {
    let id = req.params.id;

    if(utils.checkIfEmpty(id)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "id")
        );
    }
    
    if(!utils.checkIfAllNumbers(id)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "id")
        );
    }

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }

        if (req.userObj.id == id) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(1052, "PERMISSION_DENY", null)
            );
        }

        let selCurSelf = 'select rr.top from RP_User ru inner join RP_Role rr on ru.roleId = rr.id  ' +
                        'where (ru.id = ? or ru.id = ?) and ru.status = 0 and ru.isDelete = 0; ';
        let selCurVal = [req.userObj.id.toString(), id.toString()];
        let rowCurTop = await conn.execute(selCurSelf, selCurVal);
        if (/* req.userObj.id != id && */ rowCurTop[0].length < 2) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(5900, "USER_NOT_FOUND", null)
            );    
        }

        let operatorTop = rowCurTop[0][0].top;
        let targetTop = rowCurTop[0][1].top;
        if (targetTop <= operatorTop) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(1052, "PERMISSION_DENY", null)
            );
        }

        let secretKey = totp.authenticator.generateSecret();
        let sqlUpd = 'UPDATE RP_User ' +
                     'SET secretKey = ?, lastEditUser = ?, lastEditTime = utc_timestamp() ' + 
                     'WHERE id = ?;'; 

        let sqlVal = [secretKey, username, id];
        
        await conn.execute(sqlUpd, sqlVal);

        let sqlSel = "SELECT ru.id, ru.name, ru.nickname, ru.secretKey, ru.roleId, ru.status, DATE_FORMAT(ru.lastloginTime, '%Y-%m-%d %T') AS lastloginTime, DATE_FORMAT(ru.lastEditTime, '%Y-%m-%d %T') AS lastEditTime, ru.createUser, ru.lastEditUser " +
                     'FROM RP_User ru ' + 
                     'WHERE ru.id = ?;';
        
        let sqlSelVal = [id];

        let row = await conn.execute(sqlSel, sqlSelVal);
        
        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
        await conn.execute(sl.sqlSyslog, sqlLogVal);

        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, row[0][0])
        );
        
    } catch(err) {
        console.log(err);

        await conn.rollback();

        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );

    } finally {
        console.log('modifyUserTOTP release');

        conn.release();
    }
}

////  platform/user/password
async function modifyUserPwd(req, res) {

    let id = req.body.id;
    let password = req.body.password;

    if(utils.checkIfEmpty(id)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "id")
        );
    }
    
    if(!utils.checkIfAllNumbers(id)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "id")
        );
    }

    if(utils.checkIfEmpty(password)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "password")
        );
    }

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }

        if (req.userObj.id != id) {
            let selCurSelf = 'select rr.top from RP_User ru inner join RP_Role rr on ru.roleId = rr.id  ' +
                             'where (ru.id = ? or ru.id = ?) and ru.status = 0 and ru.isDelete = 0; ';
            let selCurVal = [req.userObj.id.toString(), id.toString()];
            let rowCurTop = await conn.execute(selCurSelf, selCurVal);

            if ( rowCurTop[0].length < 2) {

                await conn.rollback();

                return res.status(200).json(
                    utils.respObj(5900, "USER_NOT_FOUND", null)
                );    
            }

            let operatorTop = rowCurTop[0][0].top;
            let targetTop = rowCurTop[0][1].top;
            if (targetTop <= operatorTop) {

                await conn.rollback();

                return res.status(200).json(
                    utils.respObj(1052, "PERMISSION_DENY", null)
                );
            }
        }

        let cryptoPwd = utils.cryptoPwd(password);
        let oldPwd = await conn.execute('SELECT ru.password FROM RP_User ru WHERE ru.id = ?;', [id]);
        if (oldPwd[0].length <= 0) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(5900, "USER_NOT_FOUND", null)
            );    
        }
            
        if (oldPwd[0][0].password === cryptoPwd) {

            await conn.rollback();
            
            return res.status(200).json(
                utils.respObj(1020, "REPEAT_PASSWORD", null)
            );
        }
    
        let sqlUpd = 'UPDATE RP_User ' +
                     'SET password = ?, lastEditUser = ?, lastEditTime = utc_timestamp() ' + 
                     'WHERE id = ?;'; 
    
        let sqlVal = [cryptoPwd, username, id];
    
        await conn.execute(sqlUpd, sqlVal);
    
        let sqlSel = "SELECT ru.id, ru.name, ru.nickname, ru.secretKey, ru.roleId, ru.status, DATE_FORMAT(ru.lastloginTime, '%Y-%m-%d %T') AS lastloginTime, DATE_FORMAT(ru.lastEditTime, '%Y-%m-%d %T') AS lastEditTime, ru.createUser, ru.lastEditUser " +
                     'FROM RP_User ru ' + 
                     'WHERE ru.id = ?;';
            
        let sqlSelVal = [id];

        let row = await conn.execute(sqlSel, sqlSelVal);
        
        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
        await conn.execute(sl.sqlSyslog, sqlLogVal);

        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, row[0][0])
        );

    } catch(err) {
        console.log(err);

        await conn.rollback();

        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );

    } finally {
        console.log('modifyUserPwd release');
        
        conn.release();
    }

}


async function delUser(req, res) {

    return res.status(200).json(
        utils.respObj(-1, '靠ㄅ, no fucking in the function', null)
    );

    // let id = req.params.id;

    // if (!id) {
    //     return res.status(400).json(
    //         utils.respObj(1050, "PARAM_MISS", null)
    //     );
    // }

    // let conn = await db.getConnection();
    // await conn.beginTransaction();
    // try {
    //     let username = req.userObj.name;
    //     if (!username) {
    //         throw 'username undefined';
    //     }

    //     let sqlUpd = 'UPDATE RP_User ' + 
    //                  'SET lastEditUser = ?, lastEditTime = utc_timestamp(), isDelete = 1 ' + 
    //                  'WHERE id = ?;';

    //     let sqlVal = [username, id];

    //     await conn.execute(sqlUpd, sqlVal);
        

    //     let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
    //     await conn.execute(sl.sqlSyslog, sqlLogVal);

    //     await conn.commit();

    //     conn.release();

    //     return res.status(200).json(
    //         utils.respObj(0, null, null)
    //     );

    // } catch(err) {
    //     console.log(err);

    //     await conn.rollback();

    //     conn.release();
    //     return res.status(500).json(
    //         utils.respObj(-1, '靠ㄅ', null)
    //     );
    // }

}

module.exports = {
    getUser,
    addUser,
    modifyUserProfile,
    modifyUserPwd,
    modifyUserTOTP,
    delUser
}