const db = require('../project.modules/mysql_instance');
const sl = require('../project.api/syslog');
const utils = require('../project.utils/common');


//  platform/role
async function getRole(req, res) {

    let page = req.query.page;
    let pageSize = req.query.pageSize;

    if(!page || !pageSize) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", null)
        );
    }

    let nPage = parseInt(page);
    let nPageSize = parseInt(pageSize);
    let start = nPage * nPageSize;
    if (isNaN(start)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", null)
        );
    }

    let rtnArray = [];

    let conn = await db.getConnection();
    // await conn.beginTransaction();
    try {
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }
        let selCurTop = 'select rr.top from RP_User ru ' +
                        'inner join RP_Role rr on ru.roleId = rr.id ' +
                        'where ru.name = ? and ru.status = 0 and ru.isDelete = 0;';
        let selCurVal = [username];
        let rowCurTop = await conn.execute(selCurTop, selCurVal);
        if (rowCurTop[0].length <= 0) {
            return res.status(200).json(
                utils.respObj(5900, "USER_NOT_FOUND", null)
            );    
        }

        let curTop = rowCurTop[0][0].top;

        let cntRow = await conn.execute("SELECT count(*) as 'cnt' FROM RP_Role WHERE top > ?;", [curTop]);

        let roleSQLSyntax = "SELECT rr.id, rr.name, rr.note, rr.top, DATE_FORMAT(rr.createTime, '%Y-%m-%d %T') AS createTime FROM RP_Role rr WHERE rr.top > ? LIMIT ?, ?;";
        let roleVal = [curTop, start.toString(), nPageSize.toString()];
        let row = await conn.execute(roleSQLSyntax, roleVal);
        
        for (let i = 0; i < row[0].length; i ++) {
            let obj = row[0][i];
            let powerRows = await conn.execute('SELECT rpr.rpPowerId FROM RP_Power_Role rpr WHERE rpr.rpRoleId = ?;', [obj.id]);
            
            let powers = [];
            for (let i = 0; i < powerRows[0].length; i ++) {
                powers.push(powerRows[0][i].rpPowerId);
            }

            obj.powers = powers;

            rtnArray.push(obj);
        }
        
        let rtnObj = new Object();
        rtnObj.count = cntRow[0][0].cnt;
        rtnObj.rows = rtnArray;
        
        return res.status(200).json(
            utils.respObj(0, null, rtnObj)
        );

    } catch(err) {
        console.log(err);
 
        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );
    
    } finally {
        console.log('getRole release');

        conn.release();
    }
}

//  platform/role
async function addRole(req, res) {

    let name = req.body.name;
    let note = req.body.note;
    let top = req.body.top;
    let powers = req.body.powers;

    if(utils.checkIfEmpty(name)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "name")
        );
    }

    if (utils.checkSpecialCharacters(name)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "name")
        );
    }

    if(utils.checkIfEmpty(top)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "top")
        );
    }
    
    if(!utils.checkIfAllNumbers(top)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "top")
        );
    }

    if(!utils.checkStrLen(top, 1, 8)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "top")
        );
    }

    if(utils.checkIfEmpty(note)) {
        note = "";
    }
    
    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }

        let addTop = parseInt(top);

        let selCurTop = 'select rr.top from RP_User ru ' +
                        'inner join RP_Role rr on ru.roleId = rr.id ' +
                        'where ru.name = ? and ru.status = 0 and ru.isDelete = 0;';
        let selCurVal = [username];
        let rowCurTop = await conn.execute(selCurTop, selCurVal);
        if (rowCurTop[0].length <= 0) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(5900, "USER_NOT_FOUND", null)
            );    
        }
        
        if (addTop <= rowCurTop[0][0].top) {
            
            await conn.rollback();
            
            return res.status(200).json(
                utils.respObj(1052, 'PERMISSION_DENY', null)
            );            
        }

        let insertRoleSQL = 'INSERT INTO RP_Role(name, note, top, createUser, createTime, lastEditUser, lastEditTime)' + 
                            'VALUES(?, ?, ?, ?, utc_timestamp(), ?, utc_timestamp());';

        let insertRoleVal = [name, note, addTop, username, username];

        let row = await conn.execute(insertRoleSQL, insertRoleVal);
        let insertID = row[0].insertId;

        if (!utils.checkIfEmpty(powers)) {
            let arrayPower = Array.from(powers);
        
            let rpVal = '';
            for (let i = 0; i < arrayPower.length; i ++) {
                rpVal += '(' + insertID + ', ' + arrayPower[i] + '),';
            }

            let insertSqlRP = 'INSERT INTO RP_Power_Role(rpRoleId, rpPowerId)VALUES' + 
                              rpVal.substring(0, rpVal.length - 1) + 
                              ';';

            await conn.execute(insertSqlRP);
        }

        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
        await conn.execute(sl.sqlSyslog, sqlLogVal);

        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, null)
        );

    } catch(err) {
        console.log(err);
        
        await conn.rollback();
        
        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );
    
    } finally {
        console.log('addRole release');

        conn.release();
    }
    
}

//  platform/role
async function modifyRole(req, res) {

    let id = req.body.id;
    let name = req.body.name;
    let note = req.body.note;
    let top = req.body.top;
    let powers = req.body.powers;

    if(utils.checkIfEmpty(id)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "id")
        );
    }
    
    if(!utils.checkIfAllNumbers(id)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "id")
        );
    }

    if(utils.checkIfEmpty(name)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "name")
        );
    }

    if (utils.checkSpecialCharacters(name)) {
        return res.status(200).json(
            utils.respObj(1053, "ILLEGAL_CHARACTER", "name")
        );
    }

    if(utils.checkIfEmpty(top)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "top")
        );
    }
    
    if(!utils.checkIfAllNumbers(top)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "top")
        );
    }

    if(!utils.checkStrLen(top, 1, 8)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "top")
        );
    }

    if(utils.checkIfEmpty(note)) {
        note = "";
    }

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }

        let selCurTop = 'select rr.top from RP_User ru ' +
                        'inner join RP_Role rr on ru.roleId = rr.id ' +
                        'where ru.name = ? and ru.status = 0 and ru.isDelete = 0;';
        let selCurVal = [username];
        let rowCurTop = await conn.execute(selCurTop, selCurVal);
        if (rowCurTop[0].length <= 0) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(5900, "USER_NOT_FOUND", null)
            );    
        }
        
        let curTop = rowCurTop[0][0].top;

        let selTarTop = 'select top from RP_Role where id = ?;';
        let tarTop = await conn.execute(selTarTop, [id]);
        if (tarTop[0].length <= 0) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(5504, "ROLE_NOT_FOUND", null)
            );    
        }
        
        if (curTop >= top) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(1052, 'PERMISSION_DENY', null)
            );
        }

        if (curTop >= tarTop[0][0].top) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(1052, 'PERMISSION_DENY', null)
            );
        }

        let sqlSyntaxUPD = 'UPDATE RP_Role ' + 
                           'SET name = ?, note = ?, top = ?, lastEditUser = ?, lastEditTime = utc_timestamp() ' +
                           'WHERE id = ?; ';
        let updVal = [name, note, top, username, id];

        await conn.execute(sqlSyntaxUPD, updVal);

        await conn.execute('DELETE FROM RP_Power_Role WHERE rpRoleId = ?;', [id]);

        if (!utils.checkIfEmpty(powers)) {

            let arrayPower = Array.from(powers);

            let rpVal = '';
            for (let i = 0; i < arrayPower.length; i ++) {
                rpVal += '(' + id + ', ' + arrayPower[i] + '),';
            }
    
            let insertSqlRP = 'INSERT INTO RP_Power_Role(rpRoleId, rpPowerId)VALUES' + 
                              rpVal.substring(0, rpVal.length - 1) + 
                              ';';
            
            await conn.execute(insertSqlRP);
        }
        
        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
        await conn.execute(sl.sqlSyslog, sqlLogVal);

        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, null)
        );

    } catch(err) {
        console.log(err);
        
        await conn.rollback();
        
        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );

    } finally {
        console.log('modifyRole release');

        conn.release();
    }

}

//  platform/role
async function delRole(req, res) {

    let id = req.params.id;

    if(utils.checkIfEmpty(id)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "id")
        );
    }
    
    if(!utils.checkIfAllNumbers(id)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "id")
        );
    }

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }

        let selCurTop = 'select rr.top from RP_User ru ' +
                        'inner join RP_Role rr on ru.roleId = rr.id ' +
                        'where ru.name = ? and ru.status = 0 and ru.isDelete = 0;';
        let selCurVal = [username];
        let rowCurTop = await conn.execute(selCurTop, selCurVal);
        if (rowCurTop[0].length <= 0) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(5900, "USER_NOT_FOUND", null)
            );    
        }
        let curTop = rowCurTop[0][0].top;

        let selTarTop = 'select top from RP_Role where id = ?;';
        let tarTop = await conn.execute(selTarTop, [id]);
        if (tarTop[0].length <= 0) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(5504, "ROLE_NOT_FOUND", null)
            );    
        }

        if (curTop >= tarTop[0][0].top) {

            await conn.rollback();

            return res.status(200).json(
                utils.respObj(1052, 'PERMISSION_DENY', null)
            );
        }


        let rowsRPUser = await conn.execute('SELECT count(*) AS cnt FROM RP_User ru WHERE ru.roleId = ? AND ru.isDelete = 0;', [id]);
        let cnt = rowsRPUser[0][0].cnt;
        if (cnt === 0) {
            await conn.execute('DELETE FROM RP_Role WHERE id = ?', [id]);
            await conn.execute('DELETE FROM RP_Power_Role WHERE rpRoleId = ?;', [id]);

            let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
            await conn.execute(sl.sqlSyslog, sqlLogVal);
            
            await conn.commit();

            return res.status(200).json(
                utils.respObj(0, null, null)
            );
        }

        await conn.rollback();

        return res.status(200).json(
            utils.respObj(5503, 'ROLE_USING', null)
        );

    } catch(err) {
        console.log(err);
        
        await conn.rollback();
        
        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );
    
    } finally {
        console.log('delRole release');

        conn.release();
    }    
}

module.exports = {
    getRole,
    addRole,
    modifyRole,
    delRole
}