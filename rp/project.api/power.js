const db = require('../project.modules/mysql_instance');
const sl = require('../project.api/syslog');
const utils = require('../project.utils/common');


//  platform/menu
async function getPower(req, res) {

    let conn = await db.getConnection();
    try {
        let sqlSyntax = 'SELECT rp.id, rp.pid, rp.`action`, rp.name, rp.mark, rp.sort, rp.status ' +
                        'FROM RP_Power rp ' +
                        'WHERE rp.isDelete = 0;';

        let rows = await conn.execute(sqlSyntax);

        return res.status(200).json(
            utils.respObj(0, null, rows[0])
        );

    } catch(err) {
        console.log(err);

        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );
    
    } finally {
        console.log('getPower release');

        conn.release();
    }
}

//  platform/menu
async function addPower(req, res) {

    let pid = req.body.pid;
    let name = req.body.name;
    let code = req.body.code;
    let action = req.body.action;
    let mark = req.body.mark;
    let sort = req.body.sort;

    if(utils.checkIfEmpty(pid)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "pid")
        );
    }
    
    if(!utils.checkIfAllNumbers(pid)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "pid")
        );
    }
    
    if(utils.checkIfEmpty(sort)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "sort")
        );
    }
    
    if(!utils.checkIfAllNumbers(sort)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "sort")
        );
    }

    if(!utils.checkStrLen(sort, 1, 9)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "sort")
        );
    }

    if(utils.checkIfEmpty(name)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "name")
        );
    }

    if (utils.checkSpecialCharacters(name)) {
        return res.status(200).json(
            utils.respObj(1053, "ILLEGAL_CHARACTER", "name")
        );
    }

    if(utils.checkIfEmpty(code)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "code")
        );
    }

    if(utils.checkIfEmpty(action)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "action")
        );
    }

    if(utils.checkIfEmpty(mark)) {
        mark = null;
    }

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }

        let sqlSyntax = 'INSERT INTO RP_Power' +
                        '(pid, name, code, `action`, mark, sort, status, createUser, createTime, lastEditUser, lastEditTime, isDelete)' +
                        'VALUES(?, ?, ?, ?, ?, ?, ?, ?, utc_timestamp(), ?, utc_timestamp(), ?);';
                        
        let value = [pid, name, code, action, mark, sort, 0, username, username, 0]
        await conn.execute(sqlSyntax, value);
        
        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
        await conn.execute(sl.sqlSyslog, sqlLogVal);

        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, null)
        );

    } catch(err) {
        console.log(err);
        
        await conn.rollback();
        
        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );

    } finally {
        console.log('addPower release');

        conn.release();
    }

}

//  platform/menu
async function modifyPower(req, res) {
    
    let id = req.body.id;
    let pid = req.body.pid;
    let name = req.body.name;
    let code = req.body.code;
    let action = req.body.action;
    let mark = req.body.mark;
    let sort = req.body.sort;
    let status = req.body.status;

    if(utils.checkIfEmpty(id)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "id")
        );
    }
    
    if(!utils.checkIfAllNumbers(id)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "id")
        );
    }

    if(utils.checkIfEmpty(pid)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "pid")
        );
    }
    
    if(!utils.checkIfAllNumbers(pid)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "pid")
        );
    }

    if(utils.checkIfEmpty(name)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "name")
        );
    }

    if (utils.checkSpecialCharacters(name)) {
        return res.status(200).json(
            utils.respObj(1053, "ILLEGAL_CHARACTER", "name")
        );
    }

    if(utils.checkIfEmpty(code)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "code")
        );
    }

    if(utils.checkIfEmpty(action)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "action")
        );
    }

    if(utils.checkIfEmpty(mark)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "mark")
        );
    }

    if(utils.checkIfEmpty(sort)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "sort")
        );
    }
    
    if(!utils.checkIfAllNumbers(sort)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "sort")
        );
    }

    if(!utils.checkStrLen(sort, 1, 9)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "sort")
        );
    }

    if(utils.checkIfEmpty(status)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "status")
        );
    }
    
    if(!utils.checkIfAllNumbers(status)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "status")
        );
    }

    if(!utils.checkStrLen(status, 1, 5)) {
        return res.status(200).json(
            utils.respObj(1054, "PARAM_LEN_LIMIT", "status")
        );
    }

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }

        let sqlSyntax = 'UPDATE RP_Power ' +
                        'SET pid = ?, name = ?, code = ?, `action` = ?, mark = ?, sort = ?, status = ?, lastEditUser = ?, lastEditTime = utc_timestamp() ' +
                        'WHERE id = ?';  
        
        let value = [pid, name, code, action, mark, sort, status, username, id];
        await conn.execute(sqlSyntax, value);

        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
        await conn.execute(sl.sqlSyslog, sqlLogVal);
        
        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, null)
        );

    } catch(err) {
        console.log(err);
        
        await conn.rollback();
        
        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );

    } finally {
        console.log('modifyPower');

        conn.release();
    }

}

//  platform/menu/{id}
async function delPower(req, res) {
    
    let id = req.params.id;
    
    if(utils.checkIfEmpty(id)) {
        return res.status(200).json(
            utils.respObj(1050, "PARAM_MISS", "id")
        );
    }
    
    if(!utils.checkIfAllNumbers(id)) {
        return res.status(200).json(
            utils.respObj(1051, "PARAM_TYPE_ERROR", "id")
        );
    }

    let conn = await db.getConnection();
    await conn.beginTransaction();
    try {
        let username = req.userObj.name;
        if (!username) {
            throw 'username undefined';
        }

        let sqlSyntax = 'UPDATE RP_Power ' +
                        'SET isDelete = 1, lastEditUser = ?, lastEditTime = utc_timestamp()' +
                        'WHERE id = ?';

        await conn.execute(sqlSyntax, [username, id]);

        let sqlLogVal = [res.req.url, res.req.method, username, req.socket.remoteAddress, ''];
        await conn.execute(sl.sqlSyslog, sqlLogVal);

        await conn.commit();

        return res.status(200).json(
            utils.respObj(0, null, null)
        );

    } catch(err) {
        console.log(err);
        
        await sl.syserrlog(req, res, err.toString());

        return res.status(500).json(
            utils.respObj(-1, 'SYS_ERROR', null)
        );
    
    } finally {
        console.log('delPower');

        conn.release();
    }
}

module.exports = {
    getPower,
    addPower,
    modifyPower,
    delPower
}