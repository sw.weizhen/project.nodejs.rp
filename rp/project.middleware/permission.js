const db = require('../project.modules/mysql_instance');
const cache = require('../project.modules/redis_instance');
const utils = require('../project.utils/common');
const cfg = require('../project.config/cfg');


const permissionCtrl = async(req, res, next) => {
    
    let cacheUser = req.userObj;
    if((typeof cacheUser) === 'undefined' || cacheUser === null) {
        return res.status(200).json(
            utils.respObj(1052, 'PERMISSION_DENY', null)
        );
    }

    let loginName = cacheUser.name
    if(utils.checkIfEmpty(loginName)) {
        return res.status(200).json(
            utils.respObj(1052, 'PERMISSION_DENY', null)
        );
    }

    let conn = await db.getConnection();
    try {
        let path = req.route.path;
        let method = req.method;

        if(method === 'DELETE') {
            path = path.substring(0, path.indexOf(':') - 1)
        }

        // /platform/user/totp
        if(method === 'PUT' && path.includes('platform/user/totp')) {
            path = '/platform/user/totp';
        }

        if(method === 'GET' && path.includes('/platform/role')) {
            path = '/permission/role';
        }

        let sqlSyntax = 'SELECT COUNT(*) AS cnt FROM RP_Power rpt ' + 
                        'WHERE rpt.mark = ? ' +
                        'AND rpt.action = ? ' +
                        'AND rpt.id IN (SELECT rpr.rpPowerId FROM rp.RP_Power_Role rpr ' +
                        'WHERE rpr.rpRoleId = (SELECT ru.roleId FROM rp.RP_User ru WHERE name = ?));';

        let sqlVal = [method, path, loginName];
                        
        let rowCnt = await conn.execute(sqlSyntax, sqlVal);

        if(rowCnt[0][0].cnt === 0) {
            return res.status(200).json(
                utils.respObj(1052, 'PERMISSION_DENY', null)
            );
        }

        next();

    } catch(err) {
        console.log(err);

        return res.status(500).json(
            utils.respObj(-1, "SYS_ERROR", null)
        );
    
    } finally {
        console.log('permission release()');

        conn.release();
    }
}

module.exports = {
    permissionCtrl
}