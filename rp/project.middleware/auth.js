const cache = require('../project.modules/redis_instance');
const utils = require('../project.utils/common');
const cfg = require('../project.config/cfg');

const authenticate = async (req, res, next) => {

    const authHeader = req.headers.authorization;

    if (!authHeader) {
        return res.status(401).json(
            utils.respObj(1008, "TOKEN_MISS", null)
        );
    }

    try {
        let jsn = await cache.redisClient.get(authHeader);
        if (!jsn) {
            return res.status(200).json(
                utils.respObj(1002, "TOKEN_EXPIRED", null)
            );
        }

        let obj = JSON.parse(jsn);
        if (!obj.pass) {
            return res.status(200).json(
                utils.respObj(1011, "AUTH_NOT_ACTIVE", null)
            );
        }

        await cache.redisClient.set(authHeader, JSON.stringify(obj), {
            EX: cfg.cfg.token_expire
        });

        req.userObj = obj;

    } catch(err) {
        console.log(err);
        return res.status(500).json(
            utils.respObj(-1, "SYS_ERROR", null)
        );
    }

    next();
}


module.exports = {
    authenticate
}