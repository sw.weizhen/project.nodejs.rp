const cfgRedis = require('../project.config/cfg');

const redis = require('redis');

const redisClient  = redis.createClient({
    socket: {
        host: cfgRedis.cfg.redis.host,
        port: cfgRedis.cfg.redis.port
    }
});

(async() => {
    await redisClient.connect();
})();

redisClient.on('error', (err) => {
    console.log('redis client error(' + cfgRedis.cfg.redis.host+':' + cfgRedis.cfg.redis.port +')', err);
});

redisClient.on('ready', () => {
    console.log('redis client connected (' + cfgRedis.cfg.redis.host+':' + cfgRedis.cfg.redis.port +')');
});


module.exports = {
    redisClient
};