const cfgMysql = require('../project.config/cfg');
const dbmysql = require('mysql2');

const pool = dbmysql.createPool({
    host: cfgMysql.cfg.mysql.host,
    port: cfgMysql.cfg.mysql.port,
    user: cfgMysql.cfg.mysql.user,
    password: cfgMysql.cfg.mysql.password,
    database: cfgMysql.cfg.mysql.database,
    waitForConnections: cfgMysql.cfg.mysql.waitForConnections,
    connectionLimit: ((cfgMysql.cfg.mysql.connectionLimit < 2) ? 2 : cfgMysql.cfg.mysql.connectionLimit),
    queueLimit: cfgMysql.cfg.mysql.queueLimit,
    multipleStatements: cfgMysql.cfg.mysql.multipleStatements
});

module.exports = pool.promise();